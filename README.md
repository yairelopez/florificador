# Florificador
[![banner.png](https://i.postimg.cc/XJ7T99Gb/banner.png)](https://postimg.cc/DWHjTJPj)

FLORIFICADOR (Clasificador de flores)

Elaborado por:

2171977 Ivan Andres Mendoza Oñate 
2182061 Yaire Catalina López Santana 
2182059 Alejandro Romero Serrano

El objetivo de este proyecto es clasificar algunos tipos de flores para ayudar a algunas ciencias que estudian plantas, como lo es la fenología, este proyecto es útil en casos como facilitar ela clasificacion de flores en el proceso de la polinización y así poder investigar al respecto para mejorar el proceso.

LINK DEL DATASET: https://www.kaggle.com/datasets/bogdancretu/flower299?select=Flowers299
LINK VIDEO: https://www.youtube.com/watch?v=eTUoZRRSckU
INTELIGENCIA ARTIFICIAL II – GRUPO J1
